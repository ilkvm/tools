# Tools

This is a repository for various tools I have created for doing linguistics research which I think other people may find useful.


## LaTeX resources

### FixBib

- [fixBib.pl](projects/fixBib/README.md) - a Perl script that replaces single quotes with double quotes for `title=` and `booktitle=` lines.  I.e., replaces `title={Here is my Title}` with `title={{Here is my Title}}`.  This is to requiredf for capitalization in words other than the first within certain BibTeX styles.